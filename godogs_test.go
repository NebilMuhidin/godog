package main

import (
	"errors"
	"net/mail"
	"testing"
	"unicode"

	"github.com/cucumber/godog"
)

type UserRegistration struct {
	username     string
	email        string
	password     string
	errorMessage string
}

var registeredUsers = map[string]struct{}{"testuser": {}}

type usersCtxKey struct{}
type errCtxKey struct{}

type usersKey struct {
	name     string
	password string
}

func isValidPassword(s string) bool {
	var (
		hasMinLen  = false
		hasUpper   = false
		hasLower   = false
		hasNumber  = false
		hasSpecial = false
	)
	if len(s) >= 8 {
		hasMinLen = true
	}
	for _, char := range s {
		switch {
		case unicode.IsUpper(char):
			hasUpper = true
		case unicode.IsLower(char):
			hasLower = true
		case unicode.IsNumber(char):
			hasNumber = true
		case unicode.IsPunct(char) || unicode.IsSymbol(char):
			hasSpecial = true
		}
	}
	return hasMinLen && hasUpper && hasLower && hasNumber && hasSpecial
}

func isValidUsername(s string) bool {
	return len(s) >= 5
}

func (u *UserRegistration) aUserWithTheUsernameIsAlreadyRegistered(username string) error {
	u.username = username
	if _, exists := registeredUsers[username]; exists {
		return nil
	}
	registeredUsers[username] = struct{}{}
	return nil
}

func (u *UserRegistration) iAttemptToRegisterWithTheSameUsername() error {
	if _, exists := registeredUsers[u.username]; exists {
		u.errorMessage = "username already exists"
		return nil
	}
	return errors.New("username does not exist")
}

func (u *UserRegistration) theSystemShouldReturnAnErrorMessageIndicatingThatThe() error {
	if u.errorMessage != "username already exists" {
		return errors.New("username doesn't exist")
	}
	return nil
}

func (u *UserRegistration) iAmRegisteringWithAnInvalidEmailFormat() error {
	u.email = "nebilgmail.com"
	return nil
}

func (u *UserRegistration) iSubmitTheRegistrationForm() error {
	return nil
}

func (u *UserRegistration) theSystemShouldReturnAnErrorMessageIndicatingThatTheEmailFormatIsInvalid() error {
	_, err := mail.ParseAddress(u.email)
	if err != nil {
		u.errorMessage = "email format is invalid"
		return nil
	}

	return errors.New("email format is valid")
}

func (u *UserRegistration) iAmRegisteringWithAWeakPassword() error {
	u.password = "1234567"
	return nil
}

func (u *UserRegistration) theSystemShouldReturnAnErrorMessageIndicatingThatThePasswordIsNotStrongEnough() error {
	if !isValidPassword(u.password) {
		u.errorMessage = "password is not strong enough"
		return nil
	}
	return errors.New("password is strong enough")
}

func (u *UserRegistration) iAmRegisteringWithAUsernameLessThanCharactersLong(arg1 int) error {
	u.username = "neba"
	return nil
}

func (u *UserRegistration) theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameMustBeAtLeastCharactersLong(minLength int) error {
	if !isValidUsername(u.username) {
		u.errorMessage = "username must be at least 5 characters long"
		return nil
	}
	return errors.New("username has more than 5 characters long")
}

func (u *UserRegistration) iAmRegisteringWithAPasswordThatDoesNotMeetTheStrengthRequirements() error {
	u.password = "12345tghik"
	return nil
}

func (u *UserRegistration) theSystemShouldReturnAnErrorMessageIndicatingThePasswordRequirementsEgAtLeastCharactersLongWithAtLeastOneUppercaseLetterOneLowercaseLetterOneDigitAndOneSpecialCharacter(length int) error {
	if !isValidPassword(u.password) {
		u.errorMessage = "at least 8 characters long, with at least one uppercase letter, one lowercase letter, one digit, and one special character"
		return nil
	}
	return errors.New("strong password")
}

func (u *UserRegistration) iAmARegisteredUserWithValidCredentials() error {
	u.username = "nebil"
	u.password = "1234ABcd%^"
	return nil
}

func (u *UserRegistration) iLogInWithMyUsernameAndPassword() error {
	if !isValidPassword(u.password) || !isValidUsername(u.username) {
		return errors.New("invalid username and password")
	}
	return nil
}

func (u *UserRegistration) theSystemShouldGenerateAJWTTokenForAuthenticationAndIssueARefreshToken() error {
	_, err := CreateToken(u.username, u.password)
	if err != nil {
		return errors.New("unable to create refresh token")
	}
	return nil
}

func (u *UserRegistration) iAmAttemptingToLogInWithAnInvalidUsername() error {
	u.username = "nebil"
	return nil
}

func (u *UserRegistration) iSubmitTheLoginForm() error {
	return nil
}

func (u *UserRegistration) theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameIsNotRegistered() error {
	if !isValidUsername(u.username) {
		return errors.New("invalid username")
	}
	if _, exists := registeredUsers[u.username]; exists {
		return errors.New("username is exist")
	}
	u.errorMessage = "username is not registered"
	return nil
}

func (u *UserRegistration) iAmAttemptingToLogInWithAnInvalidPassword() error {
	u.password = "1234ABDcv$%"
	return nil
}

func (u *UserRegistration) theSystemShouldReturnAnErrorMessageIndicatingThatThePasswordIsIncorrect() error {
	if !isValidPassword(u.password) {
		return errors.New("invalid password")
	}
	if _, exists := registeredUsers[u.password]; exists {
		return errors.New("valid password")
	}
	u.errorMessage = "password is incorrect"
	return nil
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	user := &UserRegistration{}
	ctx.Step(`^a user with the username "([^"]*)" is already registered,$`,
		user.aUserWithTheUsernameIsAlreadyRegistered)
	ctx.Step(`^I attempt to register with the same username,$`,
		user.iAttemptToRegisterWithTheSameUsername)
	ctx.Step(`^the system should return an error message indicating that the username already exists.$`,
		user.theSystemShouldReturnAnErrorMessageIndicatingThatThe)

	ctx.Step(`^I am registering with an invalid email format,$`,
		user.iAmRegisteringWithAnInvalidEmailFormat)
	ctx.Step(`^I submit the registration form,$`,
		user.iSubmitTheRegistrationForm)
	ctx.Step(`^the system should return an error message indicating that the email format is invalid\.$`,
		user.theSystemShouldReturnAnErrorMessageIndicatingThatTheEmailFormatIsInvalid)

	ctx.Step(`^I am registering with a weak password,$`,
		user.iAmRegisteringWithAWeakPassword)
	ctx.Step(`^the system should return an error message indicating that the password is not strong enough\.$`,
		user.theSystemShouldReturnAnErrorMessageIndicatingThatThePasswordIsNotStrongEnough)

	ctx.Step(`^I am registering with a username less than (\d+) characters long,$`,
		user.iAmRegisteringWithAUsernameLessThanCharactersLong)
	ctx.Step(`^the system should return an error message indicating that the username must be at least (\d+) characters long\.$`,
		user.theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameMustBeAtLeastCharactersLong)

	ctx.Step(`^I am registering with a password that does not meet the strength requirements,$`,
		user.iAmRegisteringWithAPasswordThatDoesNotMeetTheStrengthRequirements)
	ctx.Step(`^the system should return an error message indicating the password requirements \(e\.g\., at least (\d+) characters long, with at least one uppercase letter, one lowercase letter, one digit, and one special character\)\.$`,
		user.theSystemShouldReturnAnErrorMessageIndicatingThePasswordRequirementsEgAtLeastCharactersLongWithAtLeastOneUppercaseLetterOneLowercaseLetterOneDigitAndOneSpecialCharacter)

	ctx.Step(`^I am a registered user with valid credentials,$`,
		user.iAmARegisteredUserWithValidCredentials)
	ctx.Step(`^I log in with my username and password,$`,
		user.iLogInWithMyUsernameAndPassword)
	ctx.Step(`^the system should generate a JWT token for authentication and issue a refresh token$`,
		user.theSystemShouldGenerateAJWTTokenForAuthenticationAndIssueARefreshToken)

	ctx.Step(`^I am attempting to log in with an invalid username,$`,
		user.iAmAttemptingToLogInWithAnInvalidUsername)
	ctx.Step(`^I submit the login form,$`,
		user.iSubmitTheLoginForm)
	ctx.Step(`^the system should return an error message indicating that the username is not registered\.$`,
		user.theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameIsNotRegistered)

	ctx.Step(`^I am attempting to log in with an invalid password,$`,
		user.iAmAttemptingToLogInWithAnInvalidPassword)
	ctx.Step(`^the system should return an error message indicating that the password is incorrect\.$`,
		user.theSystemShouldReturnAnErrorMessageIndicatingThatThePasswordIsIncorrect)

}

func TestFeatures(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"features"},
			TestingT: t, // Testing instance that will run subtests.
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run feature tests")
	}
}
